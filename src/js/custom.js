$(function(){
	var url = document.location.toString();
	
	if (url.match('#')) {
		$('#'+url.split('#')[1]).tab('show');
	} 

	$('.nav-pills .nav-item a').on('shown.bs.tab', function (e) {
		window.location.hash = e.target.hash;
	});
});